import { Component } from '@angular/core';
import {Hero} from './hero';

const HEROES: Hero[] = [
	{id: 1, name: "Alexander the Great"},
	{id: 2, name: "Alexander Bell"},
	{id: 3, name: "Mother Theresa"},
	{id: 4, name: "Galileo Galilei"},
	{id: 5, name: "Nelson Mandela"},
	{id: 6, name: "Mahatma Gandhi"},
	{id: 7, name: "Leonardo Da Vinci"},
	{id: 8, name: "Albert Einstein"}
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Heroes';
  heroes = HEROES;
  selectedHero: Hero;

	 onSelect(hero: Hero): void{
		this.selectedHero = hero;
	}
}
